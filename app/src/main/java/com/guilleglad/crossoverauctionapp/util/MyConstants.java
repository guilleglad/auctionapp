package com.guilleglad.crossoverauctionapp.util;

/**
 * Created by Guillermo García on 11-09-2016.
 */
public class MyConstants {
    public static String myTag = "MyPrefs";
    public static double priceInc = 5;
    public static int maxDays = 30;
}
