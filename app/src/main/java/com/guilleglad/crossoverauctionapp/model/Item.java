package com.guilleglad.crossoverauctionapp.model;

import com.orm.SugarRecord;



/**
 * Created by Guillermo García on 10-09-2016.
 */

public class Item extends SugarRecord {
    String item_name;
    double item_initialPrice;
    String item_startDate;
    String item_endDate;
    Long item_winningBid_id;
    Long item_buyer_id;
    Long item_seller_id;
    int item_status;
    public Item(){

    }
    public Item(String item_name,double item_initialPrice,String item_startDate,String item_endDate,Long item_seller_id){
        setItem_name(item_name);
        setItem_initialPrice(item_initialPrice);
        setItem_startDate(item_startDate);
        setItem_endDate(item_endDate);
        setItem_seller_id(item_seller_id);
        setItem_buyer_id(Long.parseLong("0"));
        setItem_winningBid_id(Long.parseLong("0"));
        setItem_status(1);


    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public double getItem_initialPrice() {
        return item_initialPrice;
    }

    public void setItem_initialPrice(double item_initialPrice) {
        this.item_initialPrice = item_initialPrice;
    }

    public String getItem_startDate() {
        return item_startDate;
    }

    public void setItem_startDate(String item_startDate) {
        this.item_startDate = item_startDate;
    }

    public String getItem_endDate() {
        return item_endDate;
    }

    public void setItem_endDate(String item_endDate) {
        this.item_endDate = item_endDate;
    }

    public Long getItem_winningBid_id() {
        return item_winningBid_id;
    }

    public void setItem_winningBid_id(Long item_winningBid) {
        this.item_winningBid_id = item_winningBid;
    }

    public Long getItem_buyer_id() {
        return item_buyer_id;
    }

    public void setItem_buyer_id(Long item_buyer) {
        this.item_buyer_id = item_buyer;
    }

    public Long getItem_seller_id() {
        return item_seller_id;
    }

    public void setItem_seller_id(Long item_seller) {
        this.item_seller_id = item_seller;
    }

    public int getItem_status() {
        return item_status;
    }

    public void setItem_status(int item_status) {
        this.item_status = item_status;
    }
}
