package com.guilleglad.crossoverauctionapp.model;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;


/**
 * Created by Guillermo García on 10-09-2016.
 */
@Table
public class User extends SugarRecord {
    private Long id;
    @Unique
    String user_nick;
    String user_password;
    @Unique
    String user_email;

    public User() {
    }

    public User(String user_nick, String user_password, String user_email){
        setUser_nick(user_nick);
        setUser_password(user_password);
        setUser_email(user_email);
    }

    public String getUser_nick() {
        return user_nick;
    }

    public void setUser_nick(String user_nick) {
        this.user_nick = user_nick;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
