package com.guilleglad.crossoverauctionapp.model;

import com.orm.SugarRecord;


/**
 * Created by Guillermo García on 11-09-2016.
 */

public class Bid extends SugarRecord {
    Long id;
    double bid_amount;
    String bid_created;
    Long bid_item;
    Long bid_user;

    public Bid(){

    }
    public Bid(double bid_amount,String bid_created,long bid_item,long bid_user){
        setBid_amount(bid_amount);
        setBid_created(bid_created);
        setBid_item(bid_item);
        setBid_user(bid_user);
    }

    public double getBid_amount() {
        return bid_amount;
    }

    public void setBid_amount(double bid_amount) {
        this.bid_amount = bid_amount;
    }

    public String getBid_created() {
        return bid_created;
    }

    public void setBid_created(String bid_created) {
        this.bid_created = bid_created;
    }

    public Long getBid_item() {
        return bid_item;
    }

    public void setBid_item(Long bid_item) {
        this.bid_item = bid_item;
    }

    public long getBid_user() {
        return bid_user;
    }

    public void setBid_user(long bid_user) {
        this.bid_user = bid_user;
    }
}
