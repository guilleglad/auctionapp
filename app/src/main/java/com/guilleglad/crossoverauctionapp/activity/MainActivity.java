package com.guilleglad.crossoverauctionapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.controller.ItemRecyclerViewAdapter;
import com.guilleglad.crossoverauctionapp.util.MyConstants;
import com.guilleglad.crossoverauctionapp.model.Item;
import com.guilleglad.crossoverauctionapp.model.User;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView text_user;
    private SharedPreferences _myData;
    private User _user;
    private StaggeredGridLayoutManager MyLayoutManager;
    private RecyclerView myRecycler;
    private ArrayList<Item> myList;
    private ItemRecyclerViewAdapter rcAdapter;
    public static long currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("General List");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I = new Intent(getApplicationContext(),NewitemActivity.class);
                startActivity(I);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        _user = getLoggedUser();
        currentUser = _user.getId();
        init_components();
    }

    private void init_components() {
        myRecycler = (RecyclerView)findViewById(R.id.myRecyclerView);
        myRecycler.setHasFixedSize(true);

//        loader();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loader();
    }
    public void loader(){
        MyLayoutManager = new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
        myRecycler.setLayoutManager(MyLayoutManager);
        myList = loadItems();
        rcAdapter = new ItemRecyclerViewAdapter(getApplicationContext(), myList,this);
        rcAdapter.notifyDataSetChanged();
        myRecycler.setAdapter(rcAdapter);
    }
    private ArrayList<Item> loadItems() {
        return  (ArrayList<Item>) Item.find(Item.class,"ITEMSTATUS= ?","1");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent I;
        if(id == R.id.logout){
            logout();
        }else if(id == R.id.exit){
            finish();
        }else if(id == R.id.itemswon){
            I = new Intent(getApplicationContext(),ListitemswonActivity.class);
            startActivity(I);
        }else if(id == R.id.yourbids){
            I = new Intent(getApplicationContext(),ListyourbidsActivity.class);
            startActivity(I);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        SharedPreferences _myData = this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor _editor = _myData.edit();
        _editor.clear().commit();
        _editor.apply();
        Toast.makeText(getApplicationContext(),"Good Bye",Toast.LENGTH_LONG).show();
        Intent I = new Intent(getApplicationContext(),LoginActivity.class);
        finish();
        startActivity(I);
    }
    public User getLoggedUser() {
        User res;
        _myData= this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        long id_login = _myData.getLong("id_login",0);
        String user_nick = _myData.getString("user_nick","");
        String user_password = _myData.getString("user_password","");
        String user_email = _myData.getString("user_email","");
        res = new User(user_nick,user_password,user_email);
        res.setId(id_login);
        return res;
    }
}
