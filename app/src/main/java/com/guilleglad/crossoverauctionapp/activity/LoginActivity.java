package com.guilleglad.crossoverauctionapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.util.MyConstants;
import com.guilleglad.crossoverauctionapp.model.User;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    static SharedPreferences _myData;
    Button btn_login,btn_register;
    EditText text_nick,text_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init_components();
        init_events();
    }

    @Override
    protected void onResume() {
        super.onResume();
        check_login();
    }

    private void check_login() {
        User _user = getLoggedUser();
        if(_user != null && !_user.getUser_nick().isEmpty() && _user.getId() != null){
            goto_main(_user);
        }
    }

    private void goto_main(User _user) {
        Toast.makeText(getApplicationContext(),"Welcome "+_user.getUser_nick(),Toast.LENGTH_LONG).show();
        setLoggedUser(_user);
        Intent I = new Intent(getApplicationContext(),MainActivity.class);
        finish();
        startActivity(I);
    }

    private void init_components() {
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_register = (Button)findViewById(R.id.btn_register);
        text_nick = (EditText)findViewById(R.id.text_nick);
        text_password = (EditText)findViewById(R.id.text_password);

    }
    private void init_events() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> _users = User.find(User.class,"USERNICK= ? and USERPASSWORD=?",text_nick.getText().toString(),text_password.getText().toString());
                if(_users.size() > 0){
                    goto_main(_users.get(0));
                }else{
                    Toast.makeText(getApplicationContext(), "Nickname or password Invalid", Toast.LENGTH_LONG).show();
                    text_nick.selectAll();
                }
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(I);
            }
        });
    }

    public User getLoggedUser() {
        User res;
        _myData= this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        long id_login = _myData.getLong("id_login",0);
        String user_nick = _myData.getString("user_nick","");
        String user_password = _myData.getString("user_password","");
        String user_email = _myData.getString("user_email","");
        res = new User(user_nick,user_password,user_email);
        res.setId(id_login);
        return res;
    }

    public void setLoggedUser(User _user) {
        _myData = this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor _editor = _myData.edit();
        _editor.putLong("id_login",_user.getId());
        _editor.putString("user_nick",_user.getUser_nick());
        _editor.putString("user_password",_user.getUser_password());
        _editor.putString("user_email",_user.getUser_email());
        _editor.commit();
    }

}
