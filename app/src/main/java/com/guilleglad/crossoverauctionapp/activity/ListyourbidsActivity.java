package com.guilleglad.crossoverauctionapp.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.controller.ItemRecyclerViewAdapter;
import com.guilleglad.crossoverauctionapp.util.MyConstants;
import com.guilleglad.crossoverauctionapp.model.Item;
import com.guilleglad.crossoverauctionapp.model.User;

import java.util.ArrayList;

public class ListyourbidsActivity extends AppCompatActivity {

    private TextView text_user;
    private SharedPreferences _myData;
    private User _user;
    private StaggeredGridLayoutManager MyLayoutManager;
    private RecyclerView myRecycler;
    private ArrayList<Item> myList;
    private ItemRecyclerViewAdapter rcAdapter;
    public static long currentUser;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Bidding Items");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        _user = getLoggedUser();
        currentUser = _user.getId();
        init_components();
    }

    private void init_components() {
        myRecycler = (RecyclerView)findViewById(R.id.myRecyclerView);
        myRecycler.setHasFixedSize(true);

//        loader();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loader();
    }
    public void loader(){
        MyLayoutManager = new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
        myRecycler.setLayoutManager(MyLayoutManager);
        myList = loadItemsWon();

        rcAdapter = new ItemRecyclerViewAdapter(getApplicationContext(), myList);
        rcAdapter.notifyDataSetChanged();
        myRecycler.setAdapter(rcAdapter);
    }
    private ArrayList<Item> loadItemsWon() {
        return  (ArrayList<Item>) Item.find(Item.class,"ITEMSELLERID=?", String.valueOf(getLoggedUser().getId()));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public User getLoggedUser() {
        User res;
        _myData= this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        long id_login = _myData.getLong("id_login",0);
        String user_nick = _myData.getString("user_nick","");
        String user_password = _myData.getString("user_password","");
        String user_email = _myData.getString("user_email","");
        res = new User(user_nick,user_password,user_email);
        res.setId(id_login);
        return res;
    }
}
