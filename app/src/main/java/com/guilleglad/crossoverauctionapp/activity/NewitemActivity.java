package com.guilleglad.crossoverauctionapp.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.util.MyConstants;
import com.guilleglad.crossoverauctionapp.model.Item;
import com.guilleglad.crossoverauctionapp.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NewitemActivity extends AppCompatActivity {

    private EditText text_itemname;
    private EditText text_itemprice;
    private EditText text_itemenddate;
    private EditText text_itemstartdate;
    private SharedPreferences _myData;
    private Calendar c;
    private SimpleDateFormat sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newitem);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveItem();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init_components();
        init_events();
    }

    private void init_components() {
        text_itemname = (EditText)findViewById(R.id.text_itemname);
        text_itemprice = (EditText)findViewById(R.id.text_itemprice);
        text_itemstartdate =(EditText)findViewById(R.id.text_itemstartdate);
        c = Calendar.getInstance();
        sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        text_itemstartdate.setText(sdf.format(c.getTime()));
        text_itemstartdate.setEnabled(false);
        text_itemenddate = (EditText)findViewById(R.id.text_itemenddate);
        Spinner spinner = (Spinner) findViewById(R.id.n_days);
        List<String> list = new ArrayList<String>();
        for(int i = 1; i< MyConstants.maxDays; i++)
            list.add(String.valueOf(i));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = Calendar.getInstance();
                text_itemstartdate.setText(sdf.format(c.getTime()));
                c.add(Calendar.DATE,Integer.parseInt(parent.getItemAtPosition(position).toString()));
                text_itemenddate.setText(sdf.format(c.getTime()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void init_events() {
    }


    private void saveItem() {
        Item item = new Item(text_itemname.getText().toString()
                ,Double.parseDouble(text_itemprice.getText().toString())
                ,text_itemstartdate.getText().toString()
                ,text_itemenddate.getText().toString()
                ,getLoggedUser().getId());
        long item_id = item.save();
        if(item_id > 0){
            Toast.makeText(getApplicationContext(),"Item Submited",Toast.LENGTH_LONG).show();
            finish();
        }
    }
    public User getLoggedUser() {
        User res;
        _myData= this.getSharedPreferences(MyConstants.myTag,getApplicationContext().MODE_PRIVATE);
        long id_login = _myData.getLong("id_login",0);
        String user_nick = _myData.getString("user_nick","");
        String user_password = _myData.getString("user_password","");
        String user_email = _myData.getString("user_email","");
        res = new User(user_nick,user_password,user_email);
        res.setId(id_login);
        return res;
    }

}
