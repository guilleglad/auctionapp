package com.guilleglad.crossoverauctionapp.activity;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.model.User;

public class RegisterActivity extends AppCompatActivity {

    private Button btn_register;
    private EditText text_nick,text_password,text_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init_components();
        init_events();
    }
    private void init_components() {
        btn_register = (Button)findViewById(R.id.btn_register);
        text_nick = (EditText)findViewById(R.id.text_nick);
        text_password = (EditText)findViewById(R.id.text_password);
        text_email = (EditText)findViewById(R.id.text_email);
    }
    private void init_events() {
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User _user = new User(text_nick.getText().toString(),text_password.getText().toString(),text_email.getText().toString());
                long id =_user.save();
                if(id > 0) {
                    Toast.makeText(getApplicationContext(), "User Registered", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        });
    }
}
