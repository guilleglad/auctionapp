package com.guilleglad.crossoverauctionapp.controller;

import android.app.Application;

import com.orm.SugarContext;


/**
 * Created by Guillermo García on 20-01-2016.
 */
public class AppController extends Application{
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstace;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstace = this;
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    public static synchronized AppController getInstance(){
        return mInstace;
    }

}
