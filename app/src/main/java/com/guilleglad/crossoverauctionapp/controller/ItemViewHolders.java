package com.guilleglad.crossoverauctionapp.controller;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.activity.MainActivity;
import com.guilleglad.crossoverauctionapp.model.Bid;
import com.guilleglad.crossoverauctionapp.model.Item;
import com.guilleglad.crossoverauctionapp.util.MyConstants;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Guillermo García on 11-09-2016.
 */
public class ItemViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView item_name;
    TextView item_initialPrice;
    TextView item_seller_id;
    TextView item_endDate;
    TextView item_id;
    private EditText input;
    public TextView item_yourbid;


    public ItemViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        setItem_name((TextView)itemView.findViewById(R.id.text_itemname));
        setItem_initialPrice((TextView)itemView.findViewById(R.id.text_itemprice));
        setItem_endDate((TextView)itemView.findViewById(R.id.text_itemenddate));
        setItem_seller_id((TextView)itemView.findViewById(R.id.text_itemseller));
        setItem_id((TextView)itemView.findViewById(R.id.text_item_id));
        item_yourbid = (TextView)itemView.findViewById(R.id.text_yourbid);

    }

    public TextView getItem_id() {
        return item_id;
    }

    public void setItem_id(TextView item_id) {
        this.item_id = item_id;
    }

    public TextView getItem_name() {
        return item_name;
    }

    public void setItem_name(TextView item_name) {
        this.item_name = item_name;
    }

    public TextView getItem_initialPrice() {
        return item_initialPrice;
    }

    public void setItem_initialPrice(TextView item_initialPrice) {
        this.item_initialPrice = item_initialPrice;
    }

    public TextView getItem_seller_id() {
        return item_seller_id;
    }

    public void setItem_seller_id(TextView item_seller_id) {
        this.item_seller_id = item_seller_id;
    }

    public TextView getItem_endDate() {
        return item_endDate;
    }

    public void setItem_endDate(TextView item_endDate) {
        this.item_endDate = item_endDate;
    }

    @Override
    public void onClick(View v) {
        final View myView = v;
        if(this.getItem_endDate().equals("Finished")){
            Toast.makeText(myView.getContext(),"Item Finished",Toast.LENGTH_LONG).show();
            return;
        }

        AlertDialog.Builder _alertbuilder = new AlertDialog.Builder(v.getContext());

        _alertbuilder.setPositiveButton(v.getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Double.valueOf(input.getText().toString()) <= Double.valueOf(input.getTag().toString())) {
                    Toast.makeText(myView.getContext(),myView.getResources().getString(R.string.error_bid_price),Toast.LENGTH_LONG).show();
                    input.selectAll();
                }else{
                    createBid(myView);
                }

            }
        });
        _alertbuilder.setNegativeButton(v.getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        _alertbuilder.setTitle(v.getResources().getString(R.string.msg_bid_title));
        _alertbuilder.setMessage(v.getResources().getString(R.string.msg_bid_doyouwant));

        input = new EditText(v.getContext());
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_CLASS_NUMBER);
        double price = Double.parseDouble(this.getItem_initialPrice().getText().toString());
        input.setText(String.valueOf(price+ MyConstants.priceInc));
        input.setTag(this.getItem_initialPrice().getText());
        input.selectAll();
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        _alertbuilder.setView(input); // uncomment this line

        AlertDialog alert = _alertbuilder.create();
        alert.show();
    }
    public void createBid(View myView){
        Calendar _calendar = Calendar.getInstance();
        Date _date = _calendar.getTime();
        Bid _bid = new Bid(Double.parseDouble(input.getText().toString()),_date.toString(),Long.parseLong(getItem_id().getText().toString()), MainActivity.currentUser);
        if(_bid.save()>0){
            List<Item> _item = Item.find(Item.class,"ID=?",getItem_id().getText().toString());
            Item _currItem = _item.get(0);
            _currItem.setItem_winningBid_id(_bid.getId());
            _currItem.save();
            Toast.makeText(myView.getContext(), "Bid Added", Toast.LENGTH_SHORT).show();
            ((MainActivity)myView.getContext()).loader();
        }
    }

}
