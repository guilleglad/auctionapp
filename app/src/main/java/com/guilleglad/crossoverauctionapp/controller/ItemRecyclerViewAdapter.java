package com.guilleglad.crossoverauctionapp.controller;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.guilleglad.crossoverauctionapp.R;
import com.guilleglad.crossoverauctionapp.activity.MainActivity;
import com.guilleglad.crossoverauctionapp.model.Bid;
import com.guilleglad.crossoverauctionapp.model.Item;
import com.guilleglad.crossoverauctionapp.model.User;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Guillermo García on 11-09-2016.
 */
public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemViewHolders>{

    private final MainActivity myActivity;
    private List<Item> itemList;
    private Context context;

    public ItemRecyclerViewAdapter(Context context, List<Item> itemList,MainActivity _activity){
        this.itemList = itemList;
        this.context = context;
        this.myActivity = _activity;
    }
    public ItemRecyclerViewAdapter(Context context, List<Item> itemList){
        this.itemList = itemList;
        this.context = context;
        this.myActivity = null;
    }
    @Override
    public ItemViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,null);
        ItemViewHolders rcv = new ItemViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ItemViewHolders _holder, int _position) {
            final ItemViewHolders holder = _holder;
            final int position = _position;
            holder.getItem_name().setText(itemList.get(position).getItem_name());
            if(itemList.get(position).getItem_winningBid_id() == 0){
                holder.getItem_initialPrice().setText(String.valueOf(itemList.get(position).getItem_initialPrice()));
                holder.item_yourbid.setVisibility(View.INVISIBLE);
            }
            else{
                List<Bid> _bid = Bid.find(Bid.class,"ID=?",String.valueOf(itemList.get(position).getItem_winningBid_id()));
                Bid currentBid =  _bid.get(0);
                holder.getItem_initialPrice().setText(String.valueOf(currentBid.getBid_amount()));
                if(currentBid.getBid_user() == MainActivity.currentUser){
                    holder.item_yourbid.setVisibility(View.VISIBLE);
                }
            }
            List<User> _seller = User.find(User.class,"ID=?",String.valueOf(itemList.get(position).getItem_seller_id()));
            holder.getItem_seller_id().setText("Seller: "+_seller.get(0).getUser_nick());
            Date newDate=null;
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
            try {
                newDate = format.parse(itemList.get(position).getItem_endDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Period period =  getTimePassedSince(newDate);
            String remaining = "";
            int days=Math.abs(period.getDays());
            int hours=Math.abs(period.getHours());
            int minutes = Math.abs(period.getMinutes());
            int seconds=Math.abs(period.getSeconds());
            int ms = seconds*1000+minutes*1000*60+hours*1000*60*60;
            if(days>0){
                remaining+=days+"d "+hours+"h ";
                holder.getItem_endDate().setText(remaining);
            }else{
                remaining+=hours+"h "+minutes+"m "+seconds+"s";
                holder.getItem_endDate().setText(remaining);
                new CountDownTimer(ms,1000) {

                    @Override
                    public void onTick(long millis) {
                        int seconds = (int) (millis / 1000) % 60 ;
                        int minutes = (int) ((millis / (1000*60)) % 60);
                        int hours   = (int) ((millis / (1000*60*60)) % 24);
                        String text = String.format("%02dh %02dm %02ds",hours,minutes,seconds);
                        holder.getItem_endDate().setText(text);
                    }
                    @Override
                    public void onFinish() {
                        holder.getItem_endDate().setText("Finished");
                        Item _currItem = Item.findById(Item.class,itemList.get(position).getId());
                        Bid _winningBid = Bid.findById(Bid.class,_currItem.getItem_winningBid_id());
                        _currItem.setItem_buyer_id(_winningBid.getBid_user());
                        _currItem.setItem_status(0);
                        _currItem.save();
                        myActivity.loader();
                    }
                }.start();
            }

            holder.getItem_id().setText(String.valueOf(itemList.get(position).getId()));

    }
    public static Period getTimePassedSince(Date initialTimestamp){
        DateTime initDT = new DateTime(initialTimestamp.getTime());
        DateTime now = new DateTime();
        Period p = new Period(initDT, now, PeriodType.dayTime()).normalizedStandard( PeriodType.dayTime());
        return p;
    }
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void clear(){
        itemList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Item> listViewItems) {

        itemList.addAll(listViewItems);
        this.notifyDataSetChanged();

    }

}
